import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import {Ingredient, NutrientFact, Product, RecipeLineItem, SupplierProduct} from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits,
    SumUnitsOfMeasure
} from "./supporting-files/helpers";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function


/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */
const sortBySupplierPrice = (a: SupplierProduct, b: SupplierProduct) =>
    GetCostPerBaseUnit(a) - GetCostPerBaseUnit(b);

const sortBySupplierProducts = ({supplierProducts: a}: Product, {supplierProducts: b}: Product) =>
    GetCostPerBaseUnit(a[0]) - GetCostPerBaseUnit(b[0]);

const getCheapestProduct = (ingredient: Ingredient): Product =>
    GetProductsForIngredient(ingredient)
        .map((product) => ({
            ...product,
            supplierProducts: product.supplierProducts
                .sort(sortBySupplierPrice)
        }))
        .sort(sortBySupplierProducts)[0]

const getNutrientFactInBaseUnits = (cheapestProduct: Product): NutrientFact[] =>
    cheapestProduct.nutrientFacts.map((nutrientFact) => GetNutrientFactInBaseUnits(nutrientFact));

const getCheapestCostByProduct = (cheapestProduct: Product): number => {
    return GetCostPerBaseUnit(cheapestProduct.supplierProducts[0]);
}

const reducerResult = {cheapestCost: 0, nutrientsAtCheapestCost: {}};

const receiptReducer = (accumulator: typeof reducerResult, currentRecipe: RecipeLineItem) => {
    let {nutrientsAtCheapestCost, cheapestCost} = accumulator;
    const {ingredient, unitOfMeasure} = currentRecipe;

    const cheapestProduct = getCheapestProduct(ingredient);
    const cheapestCostByProduct = getCheapestCostByProduct(cheapestProduct);
    const nutrientFactInBaseUnits = getNutrientFactInBaseUnits(cheapestProduct);

    cheapestCost += cheapestCostByProduct * unitOfMeasure.uomAmount;

    nutrientFactInBaseUnits.forEach(({nutrientName, quantityAmount, quantityPer}) => {
        const quantityAmountSum = nutrientsAtCheapestCost[nutrientName] !== undefined
            ? SumUnitsOfMeasure(nutrientsAtCheapestCost[nutrientName].quantityAmount, quantityAmount)
            : quantityAmount;

        if (nutrientsAtCheapestCost[nutrientName] === undefined)
            nutrientsAtCheapestCost[nutrientName] = {
                nutrientName,
                quantityAmount: {
                    uomAmount: 0,
                    uomName: null,
                    uomType: null,
                },
                quantityPer: {
                    uomAmount: 0,
                    uomName: null,
                    uomType: null,
                }
            };

        const preparedNutrient = nutrientsAtCheapestCost[nutrientName];

        preparedNutrient.quantityAmount = quantityAmountSum;
        preparedNutrient.quantityPer = quantityPer;
    })

    return {cheapestCost, nutrientsAtCheapestCost};
}

recipeData.forEach((recipe, index) => {
    const {recipeName, lineItems} = recipe;

    recipeSummary[recipeName] = lineItems.reduce(receiptReducer, reducerResult);

    //
    recipeSummary[recipeName] = {
        cheapestCost: recipeSummary[recipeName].cheapestCost,
        nutrientsAtCheapestCost: {
            Carbohydrates: recipeSummary[recipeName].nutrientsAtCheapestCost.Carbohydrates,
            Fat: recipeSummary[recipeName].nutrientsAtCheapestCost.Fat,
            Protein: recipeSummary[recipeName].nutrientsAtCheapestCost.Protein,
            Sodium: recipeSummary[recipeName].nutrientsAtCheapestCost.Sodium,
        }
    }


    // console.dir(recipeSummary, {depth: null});
    RunTest(recipeSummary);
})
